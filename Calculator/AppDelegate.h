//
//  AppDelegate.h
//  Calculator
//
//  Created by Maksim Rakhleev on 04.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

