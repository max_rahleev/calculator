//
//  MRSettingsViewController.m
//  Calculator
//
//  Created by Maksim Rakhleev on 05.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import "MRSettingsViewController.h"

@interface MRSettingsViewController ()

@end

@implementation MRSettingsViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                                    target:self
                                                                                                    action:@selector(actionCancelVC:)]];
        }
        
    }
    return self;
}

- (IBAction)actionColorSliderValueChanged:(UISlider *)sender {
    
    [self.delegate colorsValuesChangedWithRed:self.redSlider.value / 255.f
                                        green:self.greenSlider.value / 255.f
                                         blue:self.blueSlider.value / 255.f];
}


#pragma mark - Actions

- (void)actionCancelVC:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
