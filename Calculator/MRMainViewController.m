//
//  MRMainViewController.m
//  Calculator
//
//  Created by Maksim Rakhleev on 04.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import "MRMainViewController.h"
#import "MRCalcButton.h"
#import "MRSettingsViewController.h"

typedef enum : NSUInteger {
    MRNumber0,
    MRNumber1,
    MRNumber2,
    MRNumber3,
    MRNumber4,
    MRNumber5,
    MRNumber6,
    MRNumber7,
    MRNumber8,
    MRNumber9
} MRNumber;

typedef enum : NSUInteger {
    MROperatorPlus     = 1,
    MROperatorMinus    = 2,
    MROperatorMultiple = 3,
    MROperatorDivide   = 4,
} MROperator;

typedef enum : NSUInteger {
    MRCalcStateEnterFirstNumber,
    MRCalcStateEnterSecondNumber,
    MRCalcStateEnterOperator
} MRCalcState;

@interface MRMainViewController () <MRChangeColorProtocol, UIPopoverControllerDelegate>
@property (assign, nonatomic) MRCalcState state;
@property (assign, nonatomic) NSInteger firstNumber;
@property (assign, nonatomic) NSInteger secondNumber;
@property (assign, nonatomic) MROperator operator;
@property (strong, nonatomic) MRSettingsViewController *settingsVC;
@property (strong, nonatomic) UIPopoverController *settingsPopover;
@end

@implementation MRMainViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MRSettingsViewController"];
    self.settingsVC.delegate = self;
    
    [self.resultTextView setSelectable:NO];
    
    [self clearAll];
}

- (void)dealloc {
    
    self.settingsVC.delegate = nil;
    self.settingsPopover.delegate = nil;
}


#pragma mark - Actions

- (IBAction)actionNumberButtonClick:(MRCalcButton *)sender {
    
    if (self.state == MRCalcStateEnterFirstNumber) {
        
        if (![self checkIfUserCanClickThisButton:sender withNumber:self.firstNumber])
            return;
        
        self.firstNumber = [self calculateNewValueForNumber:self.firstNumber withDigit:sender.tag];
        
        [self printResult];
        
    } if (self.state == MRCalcStateEnterOperator || self.state == MRCalcStateEnterSecondNumber) {
        
        self.state = MRCalcStateEnterSecondNumber;
        
        if (![self checkIfUserCanClickThisButton:sender withNumber:self.secondNumber])
            return;
        
        self.secondNumber = [self calculateNewValueForNumber:self.secondNumber withDigit:sender.tag];
        
        [self printResult];
    }
    
}

- (IBAction)actionOperatorButtonClick:(MRCalcButton *)sender {
    
    if (self.state == MRCalcStateEnterFirstNumber || self.state == MRCalcStateEnterOperator) {
        self.state = MRCalcStateEnterOperator;
        self.operator = (MROperator)sender.tag;
        [self printResult];
    } else {
        [self actionEqualButtonClick:sender];
    }
}

- (IBAction)actionEqualButtonClick:(MRCalcButton *)sender {
    
    NSInteger result;
    
    switch (self.operator) {
        case MROperatorPlus:
            result = self.firstNumber + self.secondNumber;
            break;
        case MROperatorMinus:
            result = self.firstNumber - self.secondNumber;
            break;
        case MROperatorMultiple:
            result = self.firstNumber * self.secondNumber;
            break;
        case MROperatorDivide:
            if (self.secondNumber) {
                result = self.firstNumber / self.secondNumber;
            } else {
                [[[UIAlertView alloc] initWithTitle:@"Error"
                                            message:@"You can't divide by zero"
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
                [self clearAll];
            }
            
            break;
    }
    
    [self.resultTextView setText:[NSString stringWithFormat:@"%ld", result]];

    self.firstNumber = result;
    self.secondNumber = 0;
    self.state = MRCalcStateEnterFirstNumber;
}

- (IBAction)actionClearButtonClick:(MRCalcButton *)sender {
    
    self.firstNumber = 0;
    [self clearAll];
}

- (IBAction)actionPlusMinusButtonClick:(MRCalcButton *)sender {
    
    if (self.state == MRCalcStateEnterFirstNumber) {
        self.firstNumber *= -1;
    } else if (self.state == MRCalcStateEnterSecondNumber) {
        self.secondNumber *= -1;
    }
    
    [self printResult];
}

- (IBAction)actionSettingsButtonClicked:(MRCalcButton *)sender {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        self.settingsPopover = [[UIPopoverController alloc]initWithContentViewController:self.settingsVC];
        [self.settingsPopover presentPopoverFromRect:sender.frame
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionAny
                                            animated:YES];
        self.settingsPopover.delegate = self;
        
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:self.settingsVC];
        
        [self presentViewController:navVC animated:YES completion:nil];
    }
    
}


#pragma mark - MRChangeColorProtocol

- (void)colorsValuesChangedWithRed:(CGFloat)red
                             green:(CGFloat)green
                              blue:(CGFloat)blue {
    
    [self.view setBackgroundColor:[UIColor colorWithRed:red
                                                  green:green
                                                   blue:blue
                                                  alpha:1.f]];
}


#pragma mark - Helpers

- (void)printResult {

    NSDictionary *operators = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"+", @(MROperatorPlus),
                               @"-", @(MROperatorMinus),
                               @"x", @(MROperatorMultiple),
                               @"/", @(MROperatorDivide),
                               nil];
    
    NSDictionary *formats = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSString stringWithFormat:@"%ld", self.firstNumber], @(MRCalcStateEnterFirstNumber),
                             [NSString stringWithFormat:@"%ld %@", self.firstNumber, [operators objectForKey:@(self.operator)]], @(MRCalcStateEnterOperator),
                             [NSString stringWithFormat:@"%ld %@ %ld", self.firstNumber, [operators objectForKey:@(self.operator)], self.secondNumber],  @(MRCalcStateEnterSecondNumber),nil];
    
    [self.resultTextView setText:[formats objectForKey:@(self.state)]];
}

- (NSInteger)calculateNewValueForNumber:(NSInteger)number withDigit:(NSInteger)digit {
    
    if ([NSString stringWithFormat:@"%ld", number].length <= 18 && number < NSIntegerMax / 10 - digit) {  //NSIntegerMax = 9223372036854775807. Length = 19.
        number *= 10;
        number += digit;
    }
    return number;
}

- (BOOL)checkIfUserCanClickThisButton:(MRCalcButton *)button withNumber:(NSInteger)number {
    
    if (number) {
        return YES;
    } else if (button.tag != MRNumber0) {
        return YES;
    } else {
        return NO;
    }
}

- (void)clearAll {
    
    self.resultTextView.text = @"";
    self.secondNumber = 0;
    self.operator = 0;
    self.state = MRCalcStateEnterFirstNumber;
}

@end
