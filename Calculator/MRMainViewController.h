//
//  MRMainViewController.h
//  Calculator
//
//  Created by Maksim Rakhleev on 04.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MRCalcButton;

@interface MRMainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *resultTextView;

- (IBAction)actionNumberButtonClick:(MRCalcButton *)sender;

- (IBAction)actionOperatorButtonClick:(MRCalcButton *)sender;

- (IBAction)actionEqualButtonClick:(MRCalcButton *)sender;

- (IBAction)actionClearButtonClick:(MRCalcButton *)sender;

- (IBAction)actionPlusMinusButtonClick:(MRCalcButton *)sender;

- (IBAction)actionSettingsButtonClicked:(MRCalcButton *)sender;

@end