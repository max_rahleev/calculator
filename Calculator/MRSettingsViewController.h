//
//  MRSettingsViewController.h
//  Calculator
//
//  Created by Maksim Rakhleev on 05.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MRChangeColorProtocol;

@interface MRSettingsViewController : UIViewController

@property (weak, nonatomic) id <MRChangeColorProtocol> delegate;

@property (weak, nonatomic) IBOutlet UISlider *redSlider;
@property (weak, nonatomic) IBOutlet UISlider *greenSlider;
@property (weak, nonatomic) IBOutlet UISlider *blueSlider;

- (IBAction)actionColorSliderValueChanged:(UISlider *)sender;

@end


@protocol MRChangeColorProtocol <NSObject>
@required
- (void)colorsValuesChangedWithRed:(CGFloat)red
                             green:(CGFloat)green
                              blue:(CGFloat)blue;
@end
