//
//  MRCalcButton.m
//  Calculator
//
//  Created by Maksim Rakhleev on 04.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import "MRCalcButton.h"

@implementation MRCalcButton

- (void)drawRect:(CGRect)rect {
    
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

@end
